<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'portfolio' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '1X5Ob+%H8Soh~OLt#Wjm?<US6F[TYo~RbA&e9B-H1IA8@e-a~fBcH~=an(@Gh1n.' );
define( 'SECURE_AUTH_KEY',  'mUT.:~#8Q)`Ygx}=KRW<#2:R*5@>-W_cH)zR61DS!pL-pXa$7>Wau>7vXZcnbVZt' );
define( 'LOGGED_IN_KEY',    'Hjvp5yfHzJuO?tvkfh6?t~F,@cb7c!k$Jx-o:|H@,wM9vG*>c*Q&gsYL`/sYW<@u' );
define( 'NONCE_KEY',        'N2a16(ie-x$~;q@Mjir%PQ[tGXd!ex6E+S !9NgJPw@8ub$ja3Ny~ L58R`>+nz#' );
define( 'AUTH_SALT',        'heqDHI30K.6D!#nm=,O=!,8Yu?4%_AC&qn2c.!%`uS`DuznSmYX?BHYV6[5opN5~' );
define( 'SECURE_AUTH_SALT', 'SQq.6sA?ePNOG4F7C(B(UEp_+w5x`hl.;@STv::,0:0dtkZjtFTh{jEKhv ?q i$' );
define( 'LOGGED_IN_SALT',   '`zi*-E+.5*e3:Ft16?jqmiR/dJ3k`c|>!R7`HHdps)3>4g,4u/afG$D[1N;.4bNg' );
define( 'NONCE_SALT',       'Z4CZs;h~T`6gas!ot[KEzhcIF>vv0M2!a,#WufiV@8 qHBqD]!Ts} 7V}]|M,+!N' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
